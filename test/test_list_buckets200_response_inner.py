"""
    Garage Administration API v0+garage-v0.8.0

    Administrate your Garage cluster programatically, including status, layout, keys, buckets, and maintainance tasks.  *Disclaimer: The API is not stable yet, hence its v0 tag. The API can change at any time, and changes can include breaking backward compatibility. Read the changelog and upgrade your scripts before upgrading. Additionnaly, this specification is very early stage and can contain bugs, especially on error return codes/types that are not tested yet. Do not expect a well finished and polished product!*   # noqa: E501

    The version of the OpenAPI document: v0.8.0
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import garage_admin_sdk
from garage_admin_sdk.model.list_buckets200_response_inner_local_aliases_inner import ListBuckets200ResponseInnerLocalAliasesInner
globals()['ListBuckets200ResponseInnerLocalAliasesInner'] = ListBuckets200ResponseInnerLocalAliasesInner
from garage_admin_sdk.model.list_buckets200_response_inner import ListBuckets200ResponseInner


class TestListBuckets200ResponseInner(unittest.TestCase):
    """ListBuckets200ResponseInner unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testListBuckets200ResponseInner(self):
        """Test ListBuckets200ResponseInner"""
        # FIXME: construct object with mandatory attributes with example values
        # model = ListBuckets200ResponseInner()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
