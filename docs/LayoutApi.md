# garage_admin_sdk.LayoutApi

All URIs are relative to *http://localhost:3903/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**add_layout**](LayoutApi.md#add_layout) | **POST** /layout | Send modifications to the cluster layout
[**apply_layout**](LayoutApi.md#apply_layout) | **POST** /layout/apply | Apply staged layout
[**get_layout**](LayoutApi.md#get_layout) | **GET** /layout | Details on the current and staged layout
[**revert_layout**](LayoutApi.md#revert_layout) | **POST** /layout/revert | Clear staged layout


# **add_layout**
> ClusterLayout add_layout(node_role_change)

Send modifications to the cluster layout

Send modifications to the cluster layout. These modifications will be included in the staged role changes, visible in subsequent calls of `GET /layout`. Once the set of staged changes is satisfactory, the user may call `POST /layout/apply` to apply the changed changes, or `POST /layout/revert` to clear all of the staged changes in the layout.  Setting the capacity to `null` will configure the node as a gateway. Otherwise, capacity must be now set in bytes (before Garage 0.9 it was arbitrary weights). For example to declare 100GB, you must set `capacity: 100000000000`.  Garage uses internally the International System of Units (SI), it assumes that 1kB = 1000 bytes, and displays storage as kB, MB, GB (and not KiB, MiB, GiB that assume 1KiB = 1024 bytes). 

### Example

* Bearer Authentication (bearerAuth):

```python
import time
import garage_admin_sdk
from garage_admin_sdk.api import layout_api
from garage_admin_sdk.model.cluster_layout import ClusterLayout
from garage_admin_sdk.model.node_role_change import NodeRoleChange
from pprint import pprint
# Defining the host is optional and defaults to http://localhost:3903/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = garage_admin_sdk.Configuration(
    host = "http://localhost:3903/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = garage_admin_sdk.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with garage_admin_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = layout_api.LayoutApi(api_client)
    node_role_change = [
        NodeRoleChange(None),
    ] # [NodeRoleChange] | To add a new node to the layout or to change the configuration of an existing node, simply set the values you want (`zone`, `capacity`, and `tags`). To remove a node, simply pass the `remove: true` field. This logic is represented in OpenAPI with a \"One Of\" object.  Contrary to the CLI that may update only a subset of the fields capacity, zone and tags, when calling this API all of these values must be specified. 

    # example passing only required values which don't have defaults set
    try:
        # Send modifications to the cluster layout
        api_response = api_instance.add_layout(node_role_change)
        pprint(api_response)
    except garage_admin_sdk.ApiException as e:
        print("Exception when calling LayoutApi->add_layout: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **node_role_change** | [**[NodeRoleChange]**](NodeRoleChange.md)| To add a new node to the layout or to change the configuration of an existing node, simply set the values you want (&#x60;zone&#x60;, &#x60;capacity&#x60;, and &#x60;tags&#x60;). To remove a node, simply pass the &#x60;remove: true&#x60; field. This logic is represented in OpenAPI with a \&quot;One Of\&quot; object.  Contrary to the CLI that may update only a subset of the fields capacity, zone and tags, when calling this API all of these values must be specified.  |

### Return type

[**ClusterLayout**](ClusterLayout.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**500** | The server can not handle your request. Check your connectivity with the rest of the cluster. |  -  |
**400** | Invalid syntax or requested change |  -  |
**200** | The layout modification has been correctly staged |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **apply_layout**
> ApplyLayout200Response apply_layout(layout_version)

Apply staged layout

Applies to the cluster the layout changes currently registered as staged layout changes.  *Note: do not try to parse the `message` field of the response, it is given as an array of string specifically because its format is not stable.* 

### Example

* Bearer Authentication (bearerAuth):

```python
import time
import garage_admin_sdk
from garage_admin_sdk.api import layout_api
from garage_admin_sdk.model.layout_version import LayoutVersion
from garage_admin_sdk.model.apply_layout200_response import ApplyLayout200Response
from pprint import pprint
# Defining the host is optional and defaults to http://localhost:3903/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = garage_admin_sdk.Configuration(
    host = "http://localhost:3903/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = garage_admin_sdk.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with garage_admin_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = layout_api.LayoutApi(api_client)
    layout_version = LayoutVersion(
        version=13,
    ) # LayoutVersion | Similarly to the CLI, the body must include the version of the new layout that will be created, which MUST be 1 + the value of the currently existing layout in the cluster. 

    # example passing only required values which don't have defaults set
    try:
        # Apply staged layout
        api_response = api_instance.apply_layout(layout_version)
        pprint(api_response)
    except garage_admin_sdk.ApiException as e:
        print("Exception when calling LayoutApi->apply_layout: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **layout_version** | [**LayoutVersion**](LayoutVersion.md)| Similarly to the CLI, the body must include the version of the new layout that will be created, which MUST be 1 + the value of the currently existing layout in the cluster.  |

### Return type

[**ApplyLayout200Response**](ApplyLayout200Response.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**500** | The server can not handle your request. Check your connectivity with the rest of the cluster. |  -  |
**400** | Invalid syntax or requested change |  -  |
**200** | The staged layout has been applied as the new layout of the cluster, a rebalance has been triggered. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_layout**
> ClusterLayout get_layout()

Details on the current and staged layout

Returns the cluster's current layout, including:   - Currently configured cluster layout   - Staged changes to the cluster layout    *Capacity is given in bytes* *The info returned by this endpoint is a subset of the info returned by `GET /status`.* 

### Example

* Bearer Authentication (bearerAuth):

```python
import time
import garage_admin_sdk
from garage_admin_sdk.api import layout_api
from garage_admin_sdk.model.cluster_layout import ClusterLayout
from pprint import pprint
# Defining the host is optional and defaults to http://localhost:3903/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = garage_admin_sdk.Configuration(
    host = "http://localhost:3903/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = garage_admin_sdk.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with garage_admin_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = layout_api.LayoutApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Details on the current and staged layout
        api_response = api_instance.get_layout()
        pprint(api_response)
    except garage_admin_sdk.ApiException as e:
        print("Exception when calling LayoutApi->get_layout: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**ClusterLayout**](ClusterLayout.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**500** | The server can not answer your request because it is in a bad state  |  -  |
**200** | Returns the cluster&#39;s current cluster layout:   - Currently configured cluster layout   - Staged changes to the cluster layout  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **revert_layout**
> revert_layout(layout_version)

Clear staged layout

Clears all of the staged layout changes. 

### Example

* Bearer Authentication (bearerAuth):

```python
import time
import garage_admin_sdk
from garage_admin_sdk.api import layout_api
from garage_admin_sdk.model.layout_version import LayoutVersion
from pprint import pprint
# Defining the host is optional and defaults to http://localhost:3903/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = garage_admin_sdk.Configuration(
    host = "http://localhost:3903/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = garage_admin_sdk.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with garage_admin_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = layout_api.LayoutApi(api_client)
    layout_version = LayoutVersion(
        version=13,
    ) # LayoutVersion | Reverting the staged changes is done by incrementing the version number and clearing the contents of the staged change list. Similarly to the CLI, the body must include the incremented version number, which MUST be 1 + the value of the currently existing layout in the cluster. 

    # example passing only required values which don't have defaults set
    try:
        # Clear staged layout
        api_instance.revert_layout(layout_version)
    except garage_admin_sdk.ApiException as e:
        print("Exception when calling LayoutApi->revert_layout: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **layout_version** | [**LayoutVersion**](LayoutVersion.md)| Reverting the staged changes is done by incrementing the version number and clearing the contents of the staged change list. Similarly to the CLI, the body must include the incremented version number, which MUST be 1 + the value of the currently existing layout in the cluster.  |

### Return type

void (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**500** | The server can not handle your request. Check your connectivity with the rest of the cluster. |  -  |
**400** | Invalid syntax or requested change |  -  |
**200** | The staged layout has been cleared, you can start again sending modification from a fresh copy with &#x60;POST /layout&#x60;. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

