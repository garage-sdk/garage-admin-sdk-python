# BucketKeyInfo


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_key_id** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**permissions** | [**CreateBucketRequestLocalAliasAllow**](CreateBucketRequestLocalAliasAllow.md) |  | [optional] 
**bucket_local_aliases** | **[str]** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


