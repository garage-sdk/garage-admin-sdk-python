# KeyInfo


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** |  | [optional] 
**access_key_id** | **str** |  | [optional] 
**secret_access_key** | **str, none_type** |  | [optional] 
**permissions** | [**KeyInfoPermissions**](KeyInfoPermissions.md) |  | [optional] 
**buckets** | [**[KeyInfoBucketsInner]**](KeyInfoBucketsInner.md) |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


