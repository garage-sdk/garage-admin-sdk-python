# ListBuckets200ResponseInner


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | 
**global_aliases** | **[str]** |  | [optional] 
**local_aliases** | [**[ListBuckets200ResponseInnerLocalAliasesInner]**](ListBuckets200ResponseInnerLocalAliasesInner.md) |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


