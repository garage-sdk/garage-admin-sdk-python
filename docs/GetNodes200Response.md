# GetNodes200Response


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**node** | **str** |  | 
**garage_version** | **str** |  | 
**garage_features** | **[str]** |  | 
**rust_version** | **str** |  | 
**db_engine** | **str** |  | 
**known_nodes** | [**[NodeNetworkInfo]**](NodeNetworkInfo.md) |  | 
**layout** | [**ClusterLayout**](ClusterLayout.md) |  | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


