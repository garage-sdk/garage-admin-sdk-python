# garage_admin_sdk.BucketApi

All URIs are relative to *http://localhost:3903/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**allow_bucket_key**](BucketApi.md#allow_bucket_key) | **POST** /bucket/allow | Allow key
[**create_bucket**](BucketApi.md#create_bucket) | **POST** /bucket | Create a bucket
[**delete_bucket**](BucketApi.md#delete_bucket) | **DELETE** /bucket | Delete a bucket
[**delete_bucket_global_alias**](BucketApi.md#delete_bucket_global_alias) | **DELETE** /bucket/alias/global | Delete a global alias
[**delete_bucket_local_alias**](BucketApi.md#delete_bucket_local_alias) | **DELETE** /bucket/alias/local | Delete a local alias
[**deny_bucket_key**](BucketApi.md#deny_bucket_key) | **POST** /bucket/deny | Deny key
[**get_bucket_info**](BucketApi.md#get_bucket_info) | **GET** /bucket | Get a bucket
[**list_buckets**](BucketApi.md#list_buckets) | **GET** /bucket?list | List all buckets
[**put_bucket_global_alias**](BucketApi.md#put_bucket_global_alias) | **PUT** /bucket/alias/global | Add a global alias
[**put_bucket_local_alias**](BucketApi.md#put_bucket_local_alias) | **PUT** /bucket/alias/local | Add a local alias
[**update_bucket**](BucketApi.md#update_bucket) | **PUT** /bucket | Update a bucket


# **allow_bucket_key**
> BucketInfo allow_bucket_key(allow_bucket_key_request)

Allow key

⚠️ **DISCLAIMER**: Garage's developers are aware that this endpoint has an unconventional semantic. Be extra careful when implementing it, its behavior is not obvious.  Allows a key to do read/write/owner operations on a bucket.  Flags in permissions which have the value true will be activated. Other flags will remain unchanged (ie. they will keep their internal value).  For example, if you set read to true, the key will be allowed to read the bucket. If you set it to false, the key will keeps its previous read permission. If you want to disallow read for the key, check the DenyBucketKey operation. 

### Example

* Bearer Authentication (bearerAuth):

```python
import time
import garage_admin_sdk
from garage_admin_sdk.api import bucket_api
from garage_admin_sdk.model.bucket_info import BucketInfo
from garage_admin_sdk.model.allow_bucket_key_request import AllowBucketKeyRequest
from pprint import pprint
# Defining the host is optional and defaults to http://localhost:3903/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = garage_admin_sdk.Configuration(
    host = "http://localhost:3903/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = garage_admin_sdk.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with garage_admin_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = bucket_api.BucketApi(api_client)
    allow_bucket_key_request = AllowBucketKeyRequest(
        bucket_id="e6a14cd6a27f48684579ec6b381c078ab11697e6bc8513b72b2f5307e25fff9b",
        access_key_id="GK31c2f218a2e44f485b94239e",
        permissions=AllowBucketKeyRequestPermissions(
            read=True,
            write=True,
            owner=True,
        ),
    ) # AllowBucketKeyRequest | Aliases to put on the new bucket 

    # example passing only required values which don't have defaults set
    try:
        # Allow key
        api_response = api_instance.allow_bucket_key(allow_bucket_key_request)
        pprint(api_response)
    except garage_admin_sdk.ApiException as e:
        print("Exception when calling BucketApi->allow_bucket_key: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **allow_bucket_key_request** | [**AllowBucketKeyRequest**](AllowBucketKeyRequest.md)| Aliases to put on the new bucket  |

### Return type

[**BucketInfo**](BucketInfo.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**500** | The server can not handle your request. Check your connectivity with the rest of the cluster. |  -  |
**400** | Bad request, check your request body |  -  |
**404** | Bucket not found |  -  |
**200** | Returns exhaustive information about the bucket |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_bucket**
> BucketInfo create_bucket(create_bucket_request)

Create a bucket

Creates a new bucket, either with a global alias, a local one, or no alias at all. Technically, you can also specify both `globalAlias` and `localAlias` and that would create two aliases. 

### Example

* Bearer Authentication (bearerAuth):

```python
import time
import garage_admin_sdk
from garage_admin_sdk.api import bucket_api
from garage_admin_sdk.model.bucket_info import BucketInfo
from garage_admin_sdk.model.create_bucket_request import CreateBucketRequest
from pprint import pprint
# Defining the host is optional and defaults to http://localhost:3903/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = garage_admin_sdk.Configuration(
    host = "http://localhost:3903/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = garage_admin_sdk.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with garage_admin_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = bucket_api.BucketApi(api_client)
    create_bucket_request = CreateBucketRequest(
        global_alias="my_documents",
        local_alias=CreateBucketRequestLocalAlias(
            access_key_id="access_key_id_example",
            alias="alias_example",
            allow=CreateBucketRequestLocalAliasAllow(
                read=True,
                write=True,
                owner=True,
            ),
        ),
    ) # CreateBucketRequest | Aliases to put on the new bucket 

    # example passing only required values which don't have defaults set
    try:
        # Create a bucket
        api_response = api_instance.create_bucket(create_bucket_request)
        pprint(api_response)
    except garage_admin_sdk.ApiException as e:
        print("Exception when calling BucketApi->create_bucket: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_bucket_request** | [**CreateBucketRequest**](CreateBucketRequest.md)| Aliases to put on the new bucket  |

### Return type

[**BucketInfo**](BucketInfo.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**500** | The server can not handle your request. Check your connectivity with the rest of the cluster. |  -  |
**400** | The payload is not formatted correctly |  -  |
**200** | Returns exhaustive information about the bucket |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_bucket**
> delete_bucket(id)

Delete a bucket

Delete a bucket.Deletes a storage bucket. A bucket cannot be deleted if it is not empty.  **Warning:** this will delete all aliases associated with the bucket! 

### Example

* Bearer Authentication (bearerAuth):

```python
import time
import garage_admin_sdk
from garage_admin_sdk.api import bucket_api
from pprint import pprint
# Defining the host is optional and defaults to http://localhost:3903/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = garage_admin_sdk.Configuration(
    host = "http://localhost:3903/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = garage_admin_sdk.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with garage_admin_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = bucket_api.BucketApi(api_client)
    id = "b4018dc61b27ccb5c64ec1b24f53454bbbd180697c758c4d47a22a8921864a87" # str | The exact bucket identifier, a 32 bytes hexadecimal string

    # example passing only required values which don't have defaults set
    try:
        # Delete a bucket
        api_instance.delete_bucket(id)
    except garage_admin_sdk.ApiException as e:
        print("Exception when calling BucketApi->delete_bucket: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| The exact bucket identifier, a 32 bytes hexadecimal string |

### Return type

void (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**500** | The server can not handle your request. Check your connectivity with the rest of the cluster. |  -  |
**400** | Bucket is not empty |  -  |
**404** | Bucket not found |  -  |
**204** | Bucket has been deleted |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_bucket_global_alias**
> BucketInfo delete_bucket_global_alias(id, alias)

Delete a global alias

Delete a global alias from the target bucket 

### Example

* Bearer Authentication (bearerAuth):

```python
import time
import garage_admin_sdk
from garage_admin_sdk.api import bucket_api
from garage_admin_sdk.model.bucket_info import BucketInfo
from pprint import pprint
# Defining the host is optional and defaults to http://localhost:3903/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = garage_admin_sdk.Configuration(
    host = "http://localhost:3903/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = garage_admin_sdk.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with garage_admin_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = bucket_api.BucketApi(api_client)
    id = "e6a14cd6a27f48684579ec6b381c078ab11697e6bc8513b72b2f5307e25fff9b" # str | 
    alias = "my_documents" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Delete a global alias
        api_response = api_instance.delete_bucket_global_alias(id, alias)
        pprint(api_response)
    except garage_admin_sdk.ApiException as e:
        print("Exception when calling BucketApi->delete_bucket_global_alias: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  |
 **alias** | **str**|  |

### Return type

[**BucketInfo**](BucketInfo.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**500** | The server can not handle your request. Check your connectivity with the rest of the cluster. |  -  |
**400** | Bad request, check your request body |  -  |
**404** | Bucket not found |  -  |
**200** | Returns exhaustive information about the bucket |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_bucket_local_alias**
> BucketInfo delete_bucket_local_alias(id, access_key_id, alias)

Delete a local alias

Delete a local alias, bound to specified account, from the target bucket 

### Example

* Bearer Authentication (bearerAuth):

```python
import time
import garage_admin_sdk
from garage_admin_sdk.api import bucket_api
from garage_admin_sdk.model.bucket_info import BucketInfo
from pprint import pprint
# Defining the host is optional and defaults to http://localhost:3903/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = garage_admin_sdk.Configuration(
    host = "http://localhost:3903/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = garage_admin_sdk.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with garage_admin_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = bucket_api.BucketApi(api_client)
    id = "e6a14cd6a27f48684579ec6b381c078ab11697e6bc8513b72b2f5307e25fff9b" # str | 
    access_key_id = "GK31c2f218a2e44f485b94239e" # str | 
    alias = "my_documents" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Delete a local alias
        api_response = api_instance.delete_bucket_local_alias(id, access_key_id, alias)
        pprint(api_response)
    except garage_admin_sdk.ApiException as e:
        print("Exception when calling BucketApi->delete_bucket_local_alias: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  |
 **access_key_id** | **str**|  |
 **alias** | **str**|  |

### Return type

[**BucketInfo**](BucketInfo.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**500** | The server can not handle your request. Check your connectivity with the rest of the cluster. |  -  |
**400** | Bad request, check your request body |  -  |
**404** | Bucket not found |  -  |
**200** | Returns exhaustive information about the bucket |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **deny_bucket_key**
> BucketInfo deny_bucket_key(allow_bucket_key_request)

Deny key

⚠️ **DISCLAIMER**: Garage's developers are aware that this endpoint has an unconventional semantic. Be extra careful when implementing it, its behavior is not obvious.  Denies a key from doing read/write/owner operations on a bucket.  Flags in permissions which have the value true will be deactivated. Other flags will remain unchanged.  For example, if you set read to true, the key will be denied from reading. If you set read to false,  the key will keep its previous permissions. If you want the key to have the reading permission, check the AllowBucketKey operation. 

### Example

* Bearer Authentication (bearerAuth):

```python
import time
import garage_admin_sdk
from garage_admin_sdk.api import bucket_api
from garage_admin_sdk.model.bucket_info import BucketInfo
from garage_admin_sdk.model.allow_bucket_key_request import AllowBucketKeyRequest
from pprint import pprint
# Defining the host is optional and defaults to http://localhost:3903/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = garage_admin_sdk.Configuration(
    host = "http://localhost:3903/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = garage_admin_sdk.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with garage_admin_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = bucket_api.BucketApi(api_client)
    allow_bucket_key_request = AllowBucketKeyRequest(
        bucket_id="e6a14cd6a27f48684579ec6b381c078ab11697e6bc8513b72b2f5307e25fff9b",
        access_key_id="GK31c2f218a2e44f485b94239e",
        permissions=AllowBucketKeyRequestPermissions(
            read=True,
            write=True,
            owner=True,
        ),
    ) # AllowBucketKeyRequest | Aliases to put on the new bucket 

    # example passing only required values which don't have defaults set
    try:
        # Deny key
        api_response = api_instance.deny_bucket_key(allow_bucket_key_request)
        pprint(api_response)
    except garage_admin_sdk.ApiException as e:
        print("Exception when calling BucketApi->deny_bucket_key: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **allow_bucket_key_request** | [**AllowBucketKeyRequest**](AllowBucketKeyRequest.md)| Aliases to put on the new bucket  |

### Return type

[**BucketInfo**](BucketInfo.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**500** | The server can not handle your request. Check your connectivity with the rest of the cluster. |  -  |
**400** | Bad request, check your request body |  -  |
**404** | Bucket not found |  -  |
**200** | Returns exhaustive information about the bucket |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_bucket_info**
> BucketInfo get_bucket_info()

Get a bucket

Given a bucket identifier (`id`) or a global alias (`globalAlias`), get its information. It includes its aliases, its web configuration, keys that have some permissions on it, some statistics (number of objects, size), number of dangling multipart uploads, and its quotas (if any). 

### Example

* Bearer Authentication (bearerAuth):

```python
import time
import garage_admin_sdk
from garage_admin_sdk.api import bucket_api
from garage_admin_sdk.model.bucket_info import BucketInfo
from pprint import pprint
# Defining the host is optional and defaults to http://localhost:3903/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = garage_admin_sdk.Configuration(
    host = "http://localhost:3903/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = garage_admin_sdk.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with garage_admin_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = bucket_api.BucketApi(api_client)
    id = "b4018dc61b27ccb5c64ec1b24f53454bbbd180697c758c4d47a22a8921864a87" # str | The exact bucket identifier, a 32 bytes hexadecimal string.  Incompatible with `alias`.  (optional)
    global_alias = "my_documents" # str | The exact global alias of one of the existing buckets.  Incompatible with `id`.  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Get a bucket
        api_response = api_instance.get_bucket_info(id=id, global_alias=global_alias)
        pprint(api_response)
    except garage_admin_sdk.ApiException as e:
        print("Exception when calling BucketApi->get_bucket_info: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| The exact bucket identifier, a 32 bytes hexadecimal string.  Incompatible with &#x60;alias&#x60;.  | [optional]
 **global_alias** | **str**| The exact global alias of one of the existing buckets.  Incompatible with &#x60;id&#x60;.  | [optional]

### Return type

[**BucketInfo**](BucketInfo.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**500** | The server can not handle your request. Check your connectivity with the rest of the cluster. |  -  |
**404** | Bucket not found |  -  |
**200** | Returns exhaustive information about the bucket |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_buckets**
> [ListBuckets200ResponseInner] list_buckets()

List all buckets

List all the buckets on the cluster with their UUID and their global and local aliases. 

### Example

* Bearer Authentication (bearerAuth):

```python
import time
import garage_admin_sdk
from garage_admin_sdk.api import bucket_api
from garage_admin_sdk.model.list_buckets200_response_inner import ListBuckets200ResponseInner
from pprint import pprint
# Defining the host is optional and defaults to http://localhost:3903/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = garage_admin_sdk.Configuration(
    host = "http://localhost:3903/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = garage_admin_sdk.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with garage_admin_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = bucket_api.BucketApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # List all buckets
        api_response = api_instance.list_buckets()
        pprint(api_response)
    except garage_admin_sdk.ApiException as e:
        print("Exception when calling BucketApi->list_buckets: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**[ListBuckets200ResponseInner]**](ListBuckets200ResponseInner.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**500** | The server can not handle your request. Check your connectivity with the rest of the cluster. |  -  |
**200** | Returns the UUID of the bucket and all its aliases  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_bucket_global_alias**
> BucketInfo put_bucket_global_alias(id, alias)

Add a global alias

Add a global alias to the target bucket 

### Example

* Bearer Authentication (bearerAuth):

```python
import time
import garage_admin_sdk
from garage_admin_sdk.api import bucket_api
from garage_admin_sdk.model.bucket_info import BucketInfo
from pprint import pprint
# Defining the host is optional and defaults to http://localhost:3903/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = garage_admin_sdk.Configuration(
    host = "http://localhost:3903/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = garage_admin_sdk.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with garage_admin_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = bucket_api.BucketApi(api_client)
    id = "e6a14cd6a27f48684579ec6b381c078ab11697e6bc8513b72b2f5307e25fff9b" # str | 
    alias = "my_documents" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Add a global alias
        api_response = api_instance.put_bucket_global_alias(id, alias)
        pprint(api_response)
    except garage_admin_sdk.ApiException as e:
        print("Exception when calling BucketApi->put_bucket_global_alias: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  |
 **alias** | **str**|  |

### Return type

[**BucketInfo**](BucketInfo.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**500** | The server can not handle your request. Check your connectivity with the rest of the cluster. |  -  |
**400** | Bad request, check your request body |  -  |
**404** | Bucket not found |  -  |
**200** | Returns exhaustive information about the bucket |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_bucket_local_alias**
> BucketInfo put_bucket_local_alias(id, access_key_id, alias)

Add a local alias

Add a local alias, bound to specified account, to the target bucket 

### Example

* Bearer Authentication (bearerAuth):

```python
import time
import garage_admin_sdk
from garage_admin_sdk.api import bucket_api
from garage_admin_sdk.model.bucket_info import BucketInfo
from pprint import pprint
# Defining the host is optional and defaults to http://localhost:3903/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = garage_admin_sdk.Configuration(
    host = "http://localhost:3903/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = garage_admin_sdk.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with garage_admin_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = bucket_api.BucketApi(api_client)
    id = "e6a14cd6a27f48684579ec6b381c078ab11697e6bc8513b72b2f5307e25fff9b" # str | 
    access_key_id = "GK31c2f218a2e44f485b94239e" # str | 
    alias = "my_documents" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Add a local alias
        api_response = api_instance.put_bucket_local_alias(id, access_key_id, alias)
        pprint(api_response)
    except garage_admin_sdk.ApiException as e:
        print("Exception when calling BucketApi->put_bucket_local_alias: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  |
 **access_key_id** | **str**|  |
 **alias** | **str**|  |

### Return type

[**BucketInfo**](BucketInfo.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**500** | The server can not handle your request. Check your connectivity with the rest of the cluster. |  -  |
**400** | Bad request, check your request body |  -  |
**404** | Bucket not found |  -  |
**200** | Returns exhaustive information about the bucket |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_bucket**
> BucketInfo update_bucket(id, update_bucket_request)

Update a bucket

All fields (`websiteAccess` and `quotas`) are optional. If they are present, the corresponding modifications are applied to the bucket, otherwise nothing is changed.  In `websiteAccess`: if `enabled` is `true`, `indexDocument` must be specified. The field `errorDocument` is optional, if no error document is set a generic error message is displayed when errors happen. Conversely, if `enabled` is `false`, neither `indexDocument` nor `errorDocument` must be specified.  In `quotas`: new values of `maxSize` and `maxObjects` must both be specified, or set to `null` to remove the quotas. An absent value will be considered the same as a `null`. It is not possible to change only one of the two quotas. 

### Example

* Bearer Authentication (bearerAuth):

```python
import time
import garage_admin_sdk
from garage_admin_sdk.api import bucket_api
from garage_admin_sdk.model.bucket_info import BucketInfo
from garage_admin_sdk.model.update_bucket_request import UpdateBucketRequest
from pprint import pprint
# Defining the host is optional and defaults to http://localhost:3903/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = garage_admin_sdk.Configuration(
    host = "http://localhost:3903/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = garage_admin_sdk.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with garage_admin_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = bucket_api.BucketApi(api_client)
    id = "b4018dc61b27ccb5c64ec1b24f53454bbbd180697c758c4d47a22a8921864a87" # str | The exact bucket identifier, a 32 bytes hexadecimal string
    update_bucket_request = UpdateBucketRequest(
        website_access=UpdateBucketRequestWebsiteAccess(
            enabled=True,
            index_document="index.html",
            error_document="error/400.html",
        ),
        quotas=UpdateBucketRequestQuotas(
            max_size=19029801,
            max_objects=1,
        ),
    ) # UpdateBucketRequest | Requested changes on the bucket. Both root fields are optionals. 

    # example passing only required values which don't have defaults set
    try:
        # Update a bucket
        api_response = api_instance.update_bucket(id, update_bucket_request)
        pprint(api_response)
    except garage_admin_sdk.ApiException as e:
        print("Exception when calling BucketApi->update_bucket: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| The exact bucket identifier, a 32 bytes hexadecimal string |
 **update_bucket_request** | [**UpdateBucketRequest**](UpdateBucketRequest.md)| Requested changes on the bucket. Both root fields are optionals.  |

### Return type

[**BucketInfo**](BucketInfo.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**500** | The server can not handle your request. Check your connectivity with the rest of the cluster. |  -  |
**400** | Bad request, check your body. |  -  |
**404** | Bucket not found |  -  |
**200** | Returns exhaustive information about the bucket |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

