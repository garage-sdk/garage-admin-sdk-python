# BucketInfo


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**global_aliases** | **[str]** |  | [optional] 
**website_access** | **bool** |  | [optional] 
**website_config** | [**BucketInfoWebsiteConfig**](BucketInfoWebsiteConfig.md) |  | [optional] 
**keys** | [**[BucketKeyInfo]**](BucketKeyInfo.md) |  | [optional] 
**objects** | **int** |  | [optional] 
**bytes** | **int** |  | [optional] 
**unfinished_uploads** | **int** |  | [optional] 
**quotas** | [**BucketInfoQuotas**](BucketInfoQuotas.md) |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


