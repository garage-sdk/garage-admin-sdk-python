# GetHealth200Response


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **str** |  | 
**known_nodes** | **int** |  | 
**connected_nodes** | **int** |  | 
**storage_nodes** | **int** |  | 
**storage_nodes_ok** | **int** |  | 
**partitions** | **int** |  | 
**partitions_quorum** | **int** |  | 
**partitions_all_ok** | **int** |  | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


