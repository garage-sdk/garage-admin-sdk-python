# NodeClusterInfo


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**zone** | **str** |  | 
**tags** | **[str]** | User defined tags, put whatever makes sense for you, these tags are not interpreted by Garage  | 
**capacity** | **int, none_type** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


