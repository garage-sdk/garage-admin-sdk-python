# garage_admin_sdk.NodesApi

All URIs are relative to *http://localhost:3903/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**add_node**](NodesApi.md#add_node) | **POST** /connect | Connect a new node
[**get_health**](NodesApi.md#get_health) | **GET** /health | Cluster health report
[**get_nodes**](NodesApi.md#get_nodes) | **GET** /status | Describe cluster


# **add_node**
> [AddNode200ResponseInner] add_node(request_body)

Connect a new node

Instructs this Garage node to connect to other Garage nodes at specified `<node_id>@<net_address>`. `node_id` is generated automatically on node start. 

### Example

* Bearer Authentication (bearerAuth):

```python
import time
import garage_admin_sdk
from garage_admin_sdk.api import nodes_api
from garage_admin_sdk.model.add_node200_response_inner import AddNode200ResponseInner
from pprint import pprint
# Defining the host is optional and defaults to http://localhost:3903/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = garage_admin_sdk.Configuration(
    host = "http://localhost:3903/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = garage_admin_sdk.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with garage_admin_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = nodes_api.NodesApi(api_client)
    request_body = ["ec79480e0ce52ae26fd00c9da684e4fa56658d9c64cdcecb094e936de0bfe71f@10.0.0.11:3901","4a6ae5a1d0d33bf895f5bb4f0a418b7dc94c47c0dd2eb108d1158f3c8f60b0ff@10.0.0.12:3901"] # [str] | 

    # example passing only required values which don't have defaults set
    try:
        # Connect a new node
        api_response = api_instance.add_node(request_body)
        pprint(api_response)
    except garage_admin_sdk.ApiException as e:
        print("Exception when calling NodesApi->add_node: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request_body** | **[str]**|  |

### Return type

[**[AddNode200ResponseInner]**](AddNode200ResponseInner.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**500** | The server can not answer your request because it is in a bad state  |  -  |
**400** | Your request is malformed, check your JSON  |  -  |
**200** | The request has been handled correctly but it does not mean that all connection requests succeeded; some might have fail, you need to check the body!  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_health**
> GetHealth200Response get_health()

Cluster health report

Returns the global status of the cluster, the number of connected nodes (over the number of known ones), the number of healthy storage nodes (over the declared ones), and the number of healthy partitions (over the total). 

### Example

* Bearer Authentication (bearerAuth):

```python
import time
import garage_admin_sdk
from garage_admin_sdk.api import nodes_api
from garage_admin_sdk.model.get_health200_response import GetHealth200Response
from pprint import pprint
# Defining the host is optional and defaults to http://localhost:3903/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = garage_admin_sdk.Configuration(
    host = "http://localhost:3903/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = garage_admin_sdk.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with garage_admin_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = nodes_api.NodesApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Cluster health report
        api_response = api_instance.get_health()
        pprint(api_response)
    except garage_admin_sdk.ApiException as e:
        print("Exception when calling NodesApi->get_health: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**GetHealth200Response**](GetHealth200Response.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**500** | The server can not answer your request because it is in a bad state  |  -  |
**200** | Information about the queried node, its environment and the current layout  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_nodes**
> GetNodes200Response get_nodes()

Describe cluster

Returns the cluster's current status, including:  - ID of the node being queried and its version of the Garage daemon  - Live nodes  - Currently configured cluster layout  - Staged changes to the cluster layout  *Capacity is given in bytes* 

### Example

* Bearer Authentication (bearerAuth):

```python
import time
import garage_admin_sdk
from garage_admin_sdk.api import nodes_api
from garage_admin_sdk.model.get_nodes200_response import GetNodes200Response
from pprint import pprint
# Defining the host is optional and defaults to http://localhost:3903/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = garage_admin_sdk.Configuration(
    host = "http://localhost:3903/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = garage_admin_sdk.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with garage_admin_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = nodes_api.NodesApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Describe cluster
        api_response = api_instance.get_nodes()
        pprint(api_response)
    except garage_admin_sdk.ApiException as e:
        print("Exception when calling NodesApi->get_nodes: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**GetNodes200Response**](GetNodes200Response.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**500** | The server can not answer your request because it is in a bad state  |  -  |
**200** | Information about the queried node, its environment and the current layout  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

